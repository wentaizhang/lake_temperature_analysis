## Exploring Time Series of Lake Temperature
### Data Set
This Data set contains lake temperature from January, 1991 to December, 1999. The observations were acquired hourly which means there are 78888 observations in total. Meanwhile, the number of the missing observation, which indicated with value 99, is 2413.
### Collocation
- the missing observations are located and overwritten by the interpreted values.
- the principle frequency components were calculated and  removed from the original time series.
- the empirical covariance function was computed
- covariance model parameter estimation was estimated
- Covariance model parameters were estimated by using Least Square approach (Note: A linear combination of admissible covariance functions, by means of positive combination coefficients is still a covariance function)
- the estimation error was calculated

![](https://i.imgur.com/9MvVxQl.jpg)

![](https://i.imgur.com/9VWn6mA.jpg)

![](https://i.imgur.com/ISWiSkb.jpg)

Determine the parameters

![](https://i.imgur.com/1Ja4zXO.jpg)

![](https://i.imgur.com/8rDDiMW.jpg)

### AR model

![](https://i.imgur.com/1t2zUvp.jpg)

Pre-processed data using cubic interpolation

![](https://i.imgur.com/k5tewYv.jpg)

stand deviation of each day, month and year
- Max: 7.008℃ on 1th, Apr, 1995
Min: 0℃ (not reliable)
- Max: 4.553℃ on Feb, 1991
Min: 1.589℃ on Sep, 1992
- Max: 8.160℃ on 1991
Min: 6.649℃ on 1997

![](https://i.imgur.com/Vi7NkW3.jpg)

average temperature of each day, month and year
- High: 29.59℃ on 12th, July, 1991
Low: -4.854℃ on 7th, Feb, 1991
- High: 24.12℃ on July, 1994
Low: 2.075℃ on Feb, 1991
- High: 13.28℃ on 1991
Low: 12.15℃ on 1996

![](https://i.imgur.com/PJsZJPY.jpg)

detecting periodicity(autocorrelation)
Two main period
Average short: 1 day
Average long: 363.42 day

This makes sense, because temperature is higher in summer and daytime, lower at night and winter

![](https://i.imgur.com/UDv5HWO.jpg)

detecting periodicity(Fourier transform)
The frequencies of main components:
≈365 days, 1 days, 0.5 days, 182.6 days, 73 days…

What can be derived from the phase information?


![](https://i.imgur.com/KJ668Qc.jpg)

The autocorrelation at lag 24 hours is 0.1801, which is lower than 0.2, that means the main periodic components has been removed.

The correlogram indicates that the first 8 serial correlations appear to differ significantly from zero and they are positive. This means indicates that if a given hour has an above average temperature, then the following 8 hours will also tend to have above average temperatures. Also, if a given hour has a below average temperature, then the following 8 hours will also tend to be below average.
