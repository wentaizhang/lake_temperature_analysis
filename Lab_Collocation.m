% Load data about years from '91 to '99
firsty = 91;
lasty = 99;
for yy = firsty : lasty
    for mm = 01 : 12
        a{yy-firsty+1,mm}=load(strcat('./data_input/a',sprintf('%02d',mod(yy,100)),sprintf('%02d',mm),'tem.010.txt'));
    end
end

A=a{1,1};
for yy = 1 : (lasty-firsty+1)
    for mm = 01 : 12
        if ((yy~=1) || (mm~=1))
            A=[A;a{yy,mm}];
        end
    end
end

B = A';
a = B(:);											% temperature sequence (?C)

N = length(a);                                   % number of observations
t = (1 : N)';									% time sequence (hours)

fprintf('\n');
fprintf('Observation epochs = %d\n',N);
fprintf('Missing data = %d\n',length(find(a==99)));

% ------------------------------------------------------------------------------
% disregard Not-Available (N.A. = 99) values and plot the time series
pos = find(a~=99);
t0 = t(pos);
a0 = a(pos);
% -------------------------------------------------------------------------------
% linear interpolation of missing data
ND = find(a==99);
a1 = a;
t1 = t;
a1(ND)=interp1(t0,a0,t1(ND),'pchip');

% -------------------------------------------------------------------------------
% Frequencies do not go from 1 to N                               ( f = n/N )
% for matlab frequencies goes from -1/2 up to 1/2
% -1/2 ... + 1/2
if (mod(N,2) == 1)						% n? of obs. odd
   f = 1/N * (-(N-1)/2 : (N-1)/2)'; 		% intervall [-fs/2 , fs/2]
else									% n? of obs. even
   f = 1/N * (-N/2 : N/2-1)'; 				% intervall [-fs/2 , fs/2]
end

% fft is estimated in such a way that for f from 0 to 1/2, 
% the representation goes on the left of the main frequence
% while for f that goes from -1/2 to 0 the rapresentation goes on the right;
Fa1 = 1/N * fftshift(fft(a1));					% Fourier transformation
% -------------------------------------------------------------------------------
m = mean(a1);

% draw at 0 mean (remove the average value)
model = m;
anew =  a1 - model;
Fanew = 1/N * fftshift(fft(anew));

% calculate standard deviation (anew=residuals)
fprintf('standard deviation = %.4f degree C\n',std(anew));

% -------------------------------------------------------------------------------
% Remove the annual - dayly and semi-annual components
for i = 1 : 8
    % remove the main periodical component
    pos = find(f>0);
    fp = f(pos);
    Fanewp = Fanew(pos);
    [FanewMax, iMax] = max(abs(Fanewp)); % iMax contains the index of the highest component
    % amplitude in ?C of the 2 sinusoidal components
    aa2 = +2*real(Fanew(N/2+1+iMax));  % cos freq (real component)
    bb2 = -2*imag(Fanew(N/2+1+iMax));  % sin freq (imag component)
    % sinusoidal component
    cs2 = aa2*cos(2*pi*f(N/2+1+iMax)*t) + bb2*sin(2*pi*f(N/2+1+iMax)*t);
    model = model + cs2; % improve the model
    anew = a1 - model; % compute the residuals
    Fanew = 1/N * fftshift(fft(anew)); % Compute the new DFT on the residuals
    fprintf('\n');
    fprintf('Period T of the removed frequency component = %.4f day\n',1/(24*f(N/2+1+iMax)));
    fprintf('standard deviation = %.4f degree C\n',std(anew)); % calculate standard deviation (anew=residuals)
    fprintf('Mean Absolute Error: %f degree C\n',mean(abs(anew)));
end

figure % figure 1
subplot(2,1,1)
plot(t,a1,'-b');        % blue: original data
hold on;
plot(t,model,'-r');    % red: model;
hold off;
xlabel('Day');
ylabel('Temperature[^{\circ}C]')
legend('Actual','Model');
grid on;
subplot(2,1,2)
plot(t,anew,'-b');     % residuals
xlabel('Day');
ylabel('Residual[^{\circ}C]')
grid on;
%% -------------------------------------------------------------------------------

% Compute the empirical covariance function
N2 = N/2;
t = (0:N2)';
ecf = xcorr(anew,N2,'biased');		% empirical covariance function - biased
ecf = ecf(N2+1:2*N2+1);

figure % figure 2
plot (0:N2,ecf,'.b'); grid;
title ('Empirical covariance function');

%% -------------------------------------------------------------------------------
% covariance model parameter estimation
%    cf(t) = A*exp(-alpha*td)*cos(r*td)  A,alpha>0
    
% setting up the starting value of A
A_st01 = 2*ecf(2) - ecf(3);						% line passing on the first two covariances
A_st02 = 3*ecf(2) - 3*ecf(3) + ecf(4);           % parabola passing on the first 3 values

if ecf(3) < (ecf(2)+ecf(4))/2					% concavity up
   A_st0 = A_st01;
else
   A_st0 = A_st02;
end

sigma2st0_v = ecf(1) - A_st0;                    % estimation of the variance of the noise
ecf_05 = A_st0/2;                                % correlation lenght => f(t) = A/2
i_lcorr = find((ecf-ecf_05)<=0, 1 );           % index of the first f(t) >= A/2;
t_lcorr = t(i_lcorr-1);						   % correletion lenght t

% setting up the starting value of a (2 examples - red and magenta)
alpha1_st = log(A_st0/ecf(i_lcorr-1))/t_lcorr;
alpha2_st=alpha1_st;
n3=100;
[pksh,lcsh,w,p] = findpeaks(ecf(1:n3));
period = mean(diff(lcsh));
r_st=2*pi/period;
A1_st=mean(p);

%% -------------------------------------------------------------------------------
% estimation of the covariance model parameters using LS

i_up = n3+1;							% maximum number of empirical values to use
t_up = t(i_up);
td = t(2:i_up);						% time distance
ecf0 = ecf(2:i_up);					% values to interpolate
A2_st = A_st0-A1_st;							% estimation A
ecm = A1_st * exp(-alpha1_st*t).*cos(r_st*t)+A2_st*exp(-alpha2_st*t);			% covariance model

% figure 3
figure
plot (t(1:n3),ecf(1:n3),'.g');
hold on;
plot (t(1:n3),ecm(1:n3),'r');
grid on;
% Least square:
for i = 1 : 5
   X = [cos(r_st*td).*exp(-alpha1_st*td),...
       -A1_st*td.*exp(-alpha1_st*td).*cos(r_st*td),...
       -A1_st*sin(r_st*td).*td.*exp(-alpha1_st*td),...
       exp(-alpha2_st*td),-A2_st.*td.*exp(-alpha2_st*td)];     % design matrix
   a = A1_st*exp(-alpha1_st*td).*cos(r_st*td)+A2_st*exp(-alpha2_st*td); % array of the known terms
   beta_st = (X'*X)\X'*(ecf0-a);							   % estimation parameter
   A1_st = A1_st+beta_st(1);                                   % adjust estimation A
   alpha1_st = alpha1_st+beta_st(2);                           % adjust estimation alpha
   r_st = r_st+beta_st(3);
   A2_st = A2_st+beta_st(4);
   alpha2_st = alpha2_st+beta_st(5);
   ecm = A1_st * exp(-alpha1_st*t).*cos(r_st*t)+A2_st*exp(-alpha2_st*t);  % calculate adjusted covariance model
   plot (0:n3-1,ecm(1:n3),'--m');
end
plot (0:n3-1,ecm(1:n3),'k');
hold off;
title ('Empirical(green) and Theoretical(red=initial, black=final)');
sigma2st_v = ecf(1)-A1_st-A2_st;  % difference of emperical A and model A, ie noise A
%% -------------------------------------------------------------------------------

% estimation with collocation 
n=24*365;
y = anew(1:n);
newdate=1:n;
newmodel=model(1:n);
Cyy = toeplitz(ecm(1:n));				% covariance matrix of the signal
Cy0y0 = Cyy + eye(n)*sigma2st_v;		% covariance matrix of the observations=covar of signal + covar of noise
yst = Cyy * ((Cy0y0) \ y(1:n));		% estimated signal
est = y - yst;
fprintf('\n');
fprintf('Estimation error: \n');
fprintf('   mean  = %f \n',mean(abs(est)));
fprintf('   s.q.m. = %f \n',std(est));
fprintf('\n');

figure
plot(newdate,y,'.-b');
hold on;
grid on;
plot(newdate,yst,'.-k');
hold off;
legend('observations','estimations');

figure
subplot(2,1,1)
plot(newdate/24,newmodel+y,'.-b');
hold on;
grid on;
plot(newdate/24,newmodel+yst,'-r');
hold off;
xlabel('Days');
ylabel('Temperature[^{\circ}C]')
legend('actual','estimated');
subplot(2,1,2)
plot(newdate/24,est,'-b');
grid on;
xlabel('Days');
ylabel('Residual[^{\circ}C]')
