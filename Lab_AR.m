% Load data about years from '91 to '99
firsty = 91;
lasty = 99;
ny=lasty-firsty+1; % number of years
nm=12; % number of months
b=cell(ny,nm);
for yy = firsty : lasty
    for mm = 01 : 12
        b{yy-firsty+1,mm}=load(strcat('./data_input/a',sprintf('%02d',mod(yy,100)),sprintf('%02d',mm),'tem.010.txt'));
    end
end

A=b{1,1};
for yy = 1 : (lasty-firsty+1)
    for mm = 01 : 12
        if ((yy~=1) || (mm~=1)) 
            A=[A;b{yy,mm}];
        end
    end
end

B = A';
a = B(:);											% temperature sequence (?C)
N = length(a);                                   % number of observations
Fs=24;      % Sample rate is 1 sample every 1 hour, so 24 per day
t = (0 : N-1)';									% time sequence (hours)
ND = find(a==99);

% disregard Not-Available (N.A. = 99) values and plot the time series signal
pos = find(a~=99);  % vector of linear indices
t0 = t(pos);
a0 = a(pos);

% linear interpolation of missing data
ai = a;
ti = t;
ai(ND)=interp1(t0,a0,ti(ND),'pchip');
m = mean(ai);
figure % figure 1
plot(t0,a0,'.b')
hold on;
plot(ti(ND),ai(ND),'.r')
grid;
hold off;
xlabel('time [Day]');
ylabel('temperature[^{\circ}C]');
legend('Observation data (outliers removed)','Interpolated data');

C=B;
for i=1:N
    C(mod(i-1,24)+1,floor((i-1)/24)+1)=ai(i);
end
day_std=std(C,0,1);
day_mean=mean(C);
D=B';
c=zeros(ny,nm);
for yy = 1 : (lasty-firsty+1)
    for mm = 1 : 12
        [c(yy,mm),~]=size(b{yy,mm});
    end
end
index=zeros(ny,nm);
d=c';
for yy = 1 : (lasty-firsty+1)
    for mm = 1 : 12
        index(yy,mm)=sum(d(1:(yy-1)*12+mm));
    end
end
month_std=zeros(1,ny*nm);
month_mean=zeros(1,ny*nm);
for yy=1:(lasty-firsty+1)
    for mm=1:12
        month_std((yy-1)*12+mm)=std(ai(24*(sum(d(1:(yy-1)*12+mm-1))+1:sum(d(1:(yy-1)*12+mm)))));
        month_mean((yy-1)*12+mm)=mean(ai(24*(sum(d(1:(yy-1)*12+mm-1))+1:sum(d(1:(yy-1)*12+mm)))));
    end
end
year_std=zeros(1,ny);
year_mean=zeros(1,ny);
for yy=1:(lasty-firsty+1)
   year_std(yy)=std(ai(24*(sum(d(1:(yy-1)*12))+1:sum(d(1:yy*12))))); 
   year_mean(yy)=mean(ai(24*(sum(d(1:(yy-1)*12))+1:sum(d(1:yy*12))))); 
end
figure % figure 2, plot standard deviation
subplot(3,1,1)
t1=datetime(1991,1,1) + caldays(0:length(C)-1);
plot(t1,day_std,'b.-')
grid;
xlabel('Days');
ylabel('Std of Temperature [^{\circ}C]');
subplot(3,1,2)
t2=datetime(1991,1,1)+calmonths(0:ny*nm-1);
plot(t2,month_std,'b.-')
grid;
xlabel('Months');
ylabel('Std of Temperature [^{\circ}C]');
subplot(3,1,3)
t3=datetime(1991,1,1)+calyears(0:ny-1);
plot(t3,year_std,'b.-')
grid;
xlabel('Years');
ylabel('Std of Temperature [^{\circ}C]');

figure % figure 3, plot mean
subplot(3,1,1)
t1=datetime(1991,1,1) + caldays(0:length(C)-1);
plot(t1,day_mean,'b.-')
grid;
xlabel('Days');
ylabel('Average Temperature [^{\circ}C]');
subplot(3,1,2)
t2=datetime(1991,1,1)+calmonths(0:ny*nm-1);
plot(t2,month_mean,'b.-')
grid;
xlabel('Months');
ylabel('Average Temperature [^{\circ}C]');
subplot(3,1,3)
t3=datetime(1991,1,1)+calyears(0:ny-1);
plot(t3,year_mean,'b.-')
grid;
xlabel('Years');
ylabel('Average Temperature [^{\circ}C]');

% using autocorrelation
tempnorm=ai-mean(ai);
[autocor,lags] = xcorr(tempnorm,365*8*Fs,'coeff');
[pksh,lcsh] = findpeaks(autocor);
short = mean(diff(lcsh))/Fs;
[pklg,lclg] = findpeaks(autocor,'MinPeakDistance',ceil(short)*Fs,'MinPeakProminence',0.2);
long = mean(diff(lclg))/Fs;
figure % figure 4
plot(lags/Fs,autocor,'b')
hold on;
pks = plot(lags(lcsh)/Fs,pksh,'.r',lags(lclg)/Fs,pklg+0.05,'vk');
hold off
legend(pks,[repmat('Period: ',[2 1]) num2str([short;long],'%f')])
ylim([-1.0,1.1])
xlabel('Lag (days)')
ylabel('Autocorrelation')

% -------------------------------------------------------------------------------
% Frequencies do not go from 1 to N                               ( f = n/N )
% for matlab frequencies goes from -1/2 up to 1/2
if (mod(N,2) == 1)						% number of obs. is odd
   f = 1/N * (-(N-1)/2 : (N-1)/2)'; 		% intervall [-fs/2 , fs/2]
else									% number of obs. is even
   f = 1/N * (-N/2 : N/2-1)'; 				% intervall [-fs/2 , fs/2]
end

% fft is estimated in such a way
% that for f from 0 to 1/2 the representation goes on the left of the main frequence
% while for f that goes from -1/2 to 0 the rapresentation goes on the right;
Fa1 = 1/N * fftshift(fft(ai));					% Fourier transformation
model = m;
anew =  ai - model;
Fanew = 1/N * fftshift(fft(anew));
fprintf('standard deviation = %.4f \n',std(anew)); % calculate standard deviation (anew=residuals)

figure % figure 5
subplot(2,1,1)
plot(f,abs(Fanew),'x-b');
title('Fourier transform DC removed');
xlabel('Frequency');
ylabel('Temperature [^{\circ}C]');
subplot(2,1,2)
plot(f,unwrap(angle(Fanew)),'b')
xlabel('Frequency');
ylabel('Radians');
% Remove the annual - dayly and semi-annual components
for i = 1 : 8   % remove the main periodical component
    % find the most relevant harmonic
    pos = find(f>0);
    Fanewp = Fanew(pos); %Fanew已减去平均值
    [FanewMax, iMax] = max(abs(Fanewp)); % iMax is the index of the highest component
    % amplitude in ?C of the 2 sinusoidal components
    aa2 = +2*real(Fanew(N/2+1+iMax));  % cos freq (real component)
    bb2 = -2*imag(Fanew(N/2+1+iMax));  % sin freq (imag component)
    cs2 = aa2*cos(2*pi*f(N/2+1+iMax)*t) + bb2*sin(2*pi*f(N/2+1+iMax)*t);    % sinusoidal component
    model = model + cs2; % improve the model
    anew = ai - model; % compute the residuals
    Fanew = 1/N * fftshift(fft(anew)); % Compute the new DFT on the residuals
    fprintf('\n');
    fprintf('Period T of the removed frequency component = %.4f Day\n',1/(f(N/2+1+iMax)*24));
    fprintf('standard deviation = %.4f \n',std(anew)); % calculate standard deviation (anew=residuals)
    fprintf('Mean Absolute Error: %f degree C\n',mean(abs(anew)));
end

figure % figure 6
subplot(2,1,1)
plot(ti/Fs,ai,'-b');        % green: original data
hold on; 
plot(ti/Fs,model,'-r');    % red: model;
hold off
xlabel('Day');
ylabel('Temperature[^{\circ}C]')
legend('Actual','Model');
grid on;
subplot(2,1,2)
plot(ti/Fs,anew,'-b');     % blue: residuals
xlabel('Day');
ylabel('Residual[^{\circ}C]')
grid on;

% Analyze Serial Correlation in Residuals
figure; % figure 7
subplot(2,1,1);
autocorr(anew,100);
title('Serial Correlation of Stochastic series');
subplot(2,1,2);
parcorr(anew(1:1000),100);

% Modeling the Stochastic Component with a seasonal AR model
lags = [1 2 3 4 23 24 25 47 48 49];
Xres = lagmatrix(anew, lags);
[beta, betaci, res2] = regress(anew, Xres);

% Analyze Residuals of Regression for Serial Correlation
figure % figure 8
subplot(2,1,1);
plot(ti/24, res2);
xlabel('Day');
ylabel('Residual[^{\circ}C]')
title('Regression Residuals & Their Serial Correlation');
subplot(2,1,2);
autocorr(res2(lags(end)+1:end),50);
title('Sample Autocorrelation Function');

% Fit a Distribution to Residuals, t-location-scale distribution is a good
% fit
PD = fitdist(res2, 'tlocationscale');

% Summary of model
tempModel = struct('sinmodel', model, 'reglags', lags, 'regbeta', ...
    beta, 'dist', PD, 'presample', anew(end-lags(end)+1:end));
save TemperatureModel.mat -struct tempModel

% Simulate model
ind=2:24*365+1;
newDates = ti(ind);
simTemp = simulateTemperature(tempModel, newDates, 1);

% Plot simulation results
subplot(2,1,1);
plot((ind)/24, ai(ind),'b')
hold on;
plot((ind)/24, simTemp,'r');
hold off;
xlabel('Day');
ylabel('Temperature[^{\circ}C]');
legend('actual','simulated');
subplot(2,1,2);
error=ai(ind)-simTemp;
plot((ind)/24, error);
xlabel('Day');
ylabel('Estimation error[^{\circ}C]')
title('Error');
mean_err=mean(error);
std_err=std(error);
max_err=max(abs(error));
min_err=min(abs(error));
fprintf('\n');
fprintf('mean error: %f degree\n',mean_err);
fprintf('standard deviation of error: %f degree\n',std_err);
fprintf('maximum error: %f degree\n',max_err);
fprintf('minimum error: %f degree\n',min_err);